const express = require (`express`);

const app = express();

const PORT = 5005;

app.use(express.json())
app.use(express.urlencoded({extended:true}))

//1
app.get(`/home`, (req, res) => res.status(202).send(`Welcome to the home page`))

//3
let users = [{
    username: "johndoe",
    password: "johndoe1234"
}]
app.get(`/users`, (req, res) =>res.status(202).send(${users}))

app.delete(`delete-user`, (req, res) =>{
    res.send(`User johndoe has been deleted`)
})


app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))